package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	Connection dbConnection;
	String driver;
	String url;

	public static synchronized DBManager getInstance() {
		instance = new DBManager();
		return instance;
	}

	public Connection getConnection() throws DBException {
		try (InputStream input = new FileInputStream("app.properties")) {
			Properties prop = new Properties();
			prop.load(input);

			driver = "com.mysql.cj.jdbc.Driver";
			url = prop.getProperty("connection.url");

		} catch (IOException ex) {
			throw new DBException("Can't get DB properties", ex);
		}
		try {
			Class.forName(driver);
			dbConnection = DriverManager.getConnection(url);
		} catch (ClassNotFoundException | SQLException e) {
			throw new DBException("Can't set DB connection", e);
		}
		return dbConnection;
	}


	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (PreparedStatement statement = getConnection()
				.prepareStatement("SELECT * FROM users")){

			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String login = resultSet.getString("login");
				User user = new User();
				user.setId(id);
				user.setLogin(login);
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DBException("Can't find all users", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try (PreparedStatement statement = getConnection()
				.prepareStatement("INSERT INTO users VALUES(DEFAULT ,?)")) {
			statement.setString(1,user.getLogin());
			return statement.executeUpdate() != 0;
		} catch (SQLException e) {
			throw new DBException("Can't insert user", e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		for (User user : users) {
			try (PreparedStatement statement = getConnection()
					.prepareStatement("DELETE FROM users WHERE id = ? AND login = ?")){
				statement.setString(1,String.valueOf(user.getId()));
				statement.setString(2,user.getLogin());
				statement.execute();
				return true;
			} catch (SQLException e) {

				throw new DBException("Can't delete user", e);
			}
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		try (PreparedStatement statement = getConnection()
				.prepareStatement("SELECT * FROM users WHERE login=?")) {
			statement.setString(1, login);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
			}

		} catch (SQLException e) {
			throw new DBException("Can't get user", e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try (PreparedStatement statement = getConnection()
				.prepareStatement("SELECT * FROM teams WHERE name=?")) {
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
			}
		} catch (SQLException e) {
			throw new DBException("Can't get team", e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (PreparedStatement statement = getConnection()
				.prepareStatement("SELECT * FROM teams")){

			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				Team team = new Team();
				team.setId(id);
				team.setName(name);
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Can't find all teams", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (PreparedStatement statement = getConnection().prepareStatement("INSERT INTO teams VALUES(DEFAULT ,?)")){
			statement.setString(1, team.getName());
			return statement.executeUpdate() != 0;
		} catch (SQLException e) {
			throw new DBException("Can't insert team", e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = getConnection();

		try (PreparedStatement statement = connection.prepareStatement("INSERT INTO users_teams VALUES(?,?)")){
			connection.setAutoCommit(false);
			// Находим в БД пользователя с логином, как у данного нам пользователя
			// и берем id из БД, записываем его нашему пользователю
			user.setId(getUser(user.getLogin()).getId());
			for (Team team : teams) {
				//Тоже самое проделываем с командой
				team.setId(getTeam(team.getName()).getId());

				statement.setInt(1, user.getId());
				statement.setInt(2, team.getId());
				statement.executeUpdate();
			}
			connection.commit();
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
				throw new DBException("Transaction was rollbacked",e);
			} catch (SQLException ex) {
				throw new DBException("Can't rollback the transaction",e);
			}
		}

	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();
		try (PreparedStatement statement = getConnection()
				.prepareStatement("SELECT * FROM users_teams " +
						"INNER JOIN teams ON users_teams.team_id = teams.id " +
						"WHERE user_id = ?")) {

			statement.setInt(1,user.getId());
			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Team team = new Team();
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				team.setId(id);
				team.setName(name);

				userTeams.add(team);


			}
		} catch (SQLException e) {
			throw new DBException("Can't get teams of user", e);
		}
		System.out.println(userTeams);
		return userTeams;

	}

	public boolean deleteTeam(Team team) throws DBException {
			try (PreparedStatement statement = getConnection()
					.prepareStatement("DELETE FROM teams WHERE name = ?")){

				statement.setString(1,team.getName());
				statement.execute();
			} catch (SQLException e) {
				throw new DBException("Can't delete team", e);
			}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try (PreparedStatement statement = getConnection()
				.prepareStatement("UPDATE teams SET name = ? WHERE id = ?")){

			statement.setString(1,team.getName());
			statement.setString(2,String.valueOf(team.getId()));
			return statement.executeUpdate() !=0;
		} catch (SQLException e) {
			throw new DBException("Can't update team", e);
		}

	}

}
